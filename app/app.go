package app

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/rpsingh21/chat-server/config"
	"github.com/rpsingh21/chat-server/handler"
)

// App struct
type App struct {
	logger *log.Logger
	server *http.Server
	router *mux.Router
	rdb    *redis.Client
	config *config.Config
}

// NewApp object
func NewApp(config *config.Config, logger *log.Logger) *App {
	app := &App{
		logger: logger,
		config: config,
	}
	app.Initialization()
	app.connectRedis()
	app.setRouter()
	return app
}

// Initialization setup
func (app *App) Initialization() {
	app.router = mux.NewRouter()
	app.server = &http.Server{
		Addr:         app.config.ServerAddr, // configure the bind address
		Handler:      app.router,            // set the default handler
		ErrorLog:     app.logger,            // set the logger for the server
		ReadTimeout:  5 * time.Second,       // max time to read request from the client
		WriteTimeout: 10 * time.Second,      // max time to write response to the client
		IdleTimeout:  120 * time.Second,     // max time for connections using TCP Keep-Alive
	}
}

func (app *App) connectRedis() {
	app.rdb = redis.NewClient(&redis.Options{
		Addr: app.config.RedisAddr,
	})
}

func (app *App) setRouter() {
	app.router.Use(handler.JSONContentTypeMiddleware)
	homeAPI := handler.NewHomeAPI(app.logger, app.rdb)
	chatHandler := handler.NewChatHandler(app.logger, app.rdb)
	app.router.HandleFunc("/", homeAPI.Home)
	app.router.HandleFunc("/chat", chatHandler.ChatWebSocketHandler)
}

// Run to start server
func (app *App) Run() {
	app.logger.Println("Running server address", app.server.Addr)
	app.logger.Fatalln(app.server.ListenAndServe())
}

// Shutdown server
func (app *App) Shutdown(ctx context.Context) {
	app.logger.Print("ShutDowning server ...")
	app.server.Shutdown(ctx)
}
