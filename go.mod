module github.com/rpsingh21/chat-server

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
)
