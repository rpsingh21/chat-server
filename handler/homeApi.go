package handler

import (
	"log"
	"net/http"

	"github.com/go-redis/redis/v8"
)

// HomeAPI api
type HomeAPI struct {
	logger *log.Logger
	rdb    *redis.Client
}

// NewHomeAPI controcert
func NewHomeAPI(logger *log.Logger, rdb *redis.Client) *HomeAPI {
	return &HomeAPI{
		logger: logger,
		rdb:    rdb,
	}
}

// Home Page
func (ha *HomeAPI) Home(rw http.ResponseWriter, r *http.Request) {
	ResponseWriter(rw, http.StatusOK, "hello world", nil)
}
