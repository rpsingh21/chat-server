package handler

import (
	"log"
	"net/http"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	"github.com/rpsingh21/chat-server/models"
)

const (
	commandSubscribe = iota
	commandUnsubscribe
	commandChat
)

// upgradeConnection is the websocket upgrader from gorilla/websockets
var upgradeConnection = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}
var connectedUsers = make(map[string]*models.UserChannel)

// ChatHandler api
type ChatHandler struct {
	rdb    *redis.Client
	logger *log.Logger
}

// NewChatHandler return object
func NewChatHandler(logger *log.Logger, rdb *redis.Client) *ChatHandler {
	return &ChatHandler{
		logger: logger,
		rdb:    rdb,
	}
}

// ChatWebSocketHandler api
func (ch *ChatHandler) ChatWebSocketHandler(w http.ResponseWriter, r *http.Request) {
	wsConn, err := upgradeConnection.Upgrade(w, r, nil)
	if err != nil {
		ErrorResponseWriter(w, http.StatusInternalServerError, err)
		return
	}

	// Connect user redis channel
	username := r.URL.Query()["username"][0]
	userChannel, err := models.UserChannelConnect(ch.rdb, username, ch.logger)

	closeCh := ch.onDisconnect(wsConn, userChannel)
	go ch.listenChannelMessage(userChannel, wsConn)

loop:
	for {
		select {
		case <-closeCh:
			wsConn.Close()
			break loop
		default:
			ch.onMessage(wsConn, userChannel)
		}
	}

}

func (ch *ChatHandler) onMessage(wsConn *websocket.Conn, uc *models.UserChannel) {
	var msg models.Message

	if err := wsConn.ReadJSON(&msg); err != nil {
		handleWSError(err, wsConn)
		return
	}
	ch.logger.Println("Sending message", msg)
	switch msg.Command {
	case commandSubscribe:
		if err := uc.Subscribe(msg.Channel); err != nil {
			handleWSError(err, wsConn)
		}
	case commandUnsubscribe:
		if err := uc.Unsubscribe(msg.Channel); err != nil {
			handleWSError(err, wsConn)
		}
	case commandChat:
		if err := uc.Chat(msg.Channel, msg.Content); err != nil {
			handleWSError(err, wsConn)
		}
		ch.logger.Println("Message sent to redis successfully")
	}
}

func (ch *ChatHandler) onDisconnect(wsConn *websocket.Conn, uc *models.UserChannel) chan struct{} {
	closeCh := make(chan struct{})
	wsConn.SetCloseHandler(func(code int, text string) error {
		ch.logger.Println("Got Close signal")
		if err := uc.Disconnect(); err != nil {
			return nil
		}
		close(closeCh)
		return nil
	})
	return closeCh
}

func (ch *ChatHandler) listenChannelMessage(uc *models.UserChannel, wsConn *websocket.Conn) {
	for m := range uc.MessageChan {
		mgs := &models.Message{
			Content: m.Payload,
			Channel: m.Channel,
		}
		ch.logger.Println("Resive message : ", mgs)
		if err := wsConn.WriteJSON(mgs); err != nil {
			ch.logger.Fatalln(err)
		}
	}
}

func handleWSError(err error, conn *websocket.Conn) {
	_ = conn.WriteJSON(models.Message{Err: err.Error()})
}
