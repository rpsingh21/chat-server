package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rpsingh21/chat-server/app"
	"github.com/rpsingh21/chat-server/config"
)

func main() {
	conf := config.NewConfig()
	logger := log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)
	app := app.NewApp(conf, logger)
	app.Run()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)

	sig := <-sigs
	logger.Printf("Got interupt signal %v \n", sig)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	app.Shutdown(ctx)
}
