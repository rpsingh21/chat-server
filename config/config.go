package config

// Config of app
type Config struct {
	ServerAddr string
	RedisAddr  string
}

// NewConfig of application
func NewConfig() *Config {
	return &Config{
		ServerAddr: "0.0.0.0:8000",
		RedisAddr:  "0.0.0.0:6379",
	}
}
