package models

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

// Const
const (
	usersKey       = "users"
	userChannelFmt = "user:%s:channels"
	ChannelsKey    = "channels"
)

// UserChannel hold user redis and websocket channerls
type UserChannel struct {
	userID           string        // User unique id
	pubSub           *redis.PubSub // Redis subscribe command connection
	stopListenerChan chan struct{} // Golang channel used to stop the current goroutine
	listening        bool          // listening to update user isActive
	rdb              *redis.Client
	MessageChan      chan redis.Message // Golang channel that communicates between the Reids
	logger           *log.Logger
}

// UserChannelConnect user to user's channels on redis
func UserChannelConnect(rdb *redis.Client, userID string, logger *log.Logger) (*UserChannel, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if _, err := rdb.SAdd(ctx, usersKey, userID).Result(); err != nil {
		logger.Fatalln(err)
		return nil, err
	}
	userChannel := &UserChannel{
		userID:           userID,
		stopListenerChan: make(chan struct{}),
		MessageChan:      make(chan redis.Message),
		rdb:              rdb,
		logger:           logger,
	}
	if err := userChannel.connect(); err != nil {
		return nil, err
	}
	return userChannel, nil
}

// Subscribe channel
func (uc *UserChannel) Subscribe(channel string) error {

	userChannelsKey := fmt.Sprintf(userChannelFmt, uc.userID)
	uc.logger.Println(userChannelsKey)

	if uc.rdb.SIsMember(context.TODO(), userChannelsKey, channel).Val() {
		return nil
	}
	if err := uc.rdb.SAdd(context.TODO(), userChannelsKey, channel).Err(); err != nil {
		return err
	}

	return uc.connect()
}

// Unsubscribe channel
func (uc *UserChannel) Unsubscribe(channel string) error {

	userChannelsKey := fmt.Sprintf(userChannelFmt, uc.userID)

	if !uc.rdb.SIsMember(context.TODO(), userChannelsKey, channel).Val() {
		return nil
	}
	if err := uc.rdb.SRem(context.TODO(), userChannelsKey, channel).Err(); err != nil {
		return err
	}

	return uc.connect()
}

// Disconnect for channel
func (uc *UserChannel) Disconnect() error {
	if uc.pubSub != nil {
		if err := uc.pubSub.Unsubscribe(context.TODO()); err != nil {
			return err
		}
		if err := uc.pubSub.Close(); err != nil {
			return err
		}
	}
	if uc.listening {
		uc.stopListenerChan <- struct{}{}
	}
	return nil
}

// Close channels
func (uc *UserChannel) Close() error {
	uc.Disconnect()
	close(uc.MessageChan)
	return nil
}

func (uc *UserChannel) connect() error {
	var c []string
	c1, err := uc.rdb.SMembers(context.TODO(), ChannelsKey).Result()
	if err != nil {
		return err
	}
	c = append(c, c1...)

	// get all user channels (from DB) and start subscribe
	c2, err := uc.rdb.SMembers(context.TODO(), fmt.Sprintf(userChannelFmt, uc.userID)).Result()
	if err != nil {
		return err
	}
	c = append(c, c2...)
	if len(c) == 0 {
		uc.logger.Println("no channels to connect to for user: ", uc.userID)
		return nil
	}
	uc.Disconnect()
	return uc.doConnect(c)
}

func (uc *UserChannel) doConnect(channels []string) error {
	uc.pubSub = uc.rdb.Subscribe(context.TODO(), channels...)

	go func() {
		uc.listening = true
		uc.logger.Println("user start to listen on chanles", channels)
		for {
			select {
			case mgs := <-uc.pubSub.Channel():
				uc.MessageChan <- *mgs
			case <-uc.stopListenerChan:
				uc.logger.Println("Stop listen redis channel")
				return
			}
		}
	}()

	return nil
}

// Chat send message
func (uc *UserChannel) Chat(channel string, content string) error {
	uc.logger.Println("redis publishd message to", channel, content)
	return uc.rdb.Publish(context.TODO(), channel, content).Err()
}

// List return list of members
func List(rdb *redis.Client) ([]string, error) {
	return rdb.SMembers(context.TODO(), usersKey).Result()
}

// GetChannels return channel
func GetChannels(rdb *redis.Client, username string) ([]string, error) {

	if !rdb.SIsMember(context.TODO(), usersKey, username).Val() {
		return nil, errors.New("user not exists")
	}

	var c []string

	c1, err := rdb.SMembers(context.TODO(), ChannelsKey).Result()
	if err != nil {
		return nil, err
	}
	c = append(c, c1...)

	// get all user channels (from DB) and start subscribe
	c2, err := rdb.SMembers(context.TODO(), fmt.Sprintf(userChannelFmt, username)).Result()
	if err != nil {
		return nil, err
	}
	c = append(c, c2...)

	return c, nil
}
